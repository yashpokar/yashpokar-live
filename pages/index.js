import Head from 'next/head';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBlog } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin, faGithub, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';

export default function Home() {
  return (
    <div className="antialiased font-normal font-roboto text-gray-800">
    	<Head>
    		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
    		<title>Yash Pokar dot com</title>
    		<meta name="og:title" property="og:title" content="Yash Pokar dot com" />
    		<meta name="description" content="Yash Pokar is a Full stack developer" />

    		<link rel="shortcut icon" href="/favicon.ico" />
			<link rel="icon" type="image/x-icon" href="/favicon.ico" />
			<link rel="icon" type="image/png" href="/favicon-16x16.png" />

			<link rel="apple-touch-icon" href="/apple-touch-icon.png" />

			<meta name="theme-color" content="#000000" />

			<link href="https://fonts.googleapis.com/css2?family=Aladin&Roboto:wght@300;400;500&display=swap" rel="stylesheet" />
    		<script src='/js/ga.js'></script>
    	</Head>

    	<div className="min-h-screen flex flex-col justify-center items-center w-full bg-primary px-2 md:px-0">
	    	<img
	    		src="https://realboag.sirv.com/Photos/ezgif.com-gif-maker.jpg"
	    		alt="Yash Pokar"
	    		className="w-32 h-32 md:w-40 md:h-40 rounded-full mb-10 cursor-pointer"
	    	/>

	    	<div className="flex flex-col bg-white px-8 md:px-16 py-3 md:py-4 rounded-md shadow-lg">
	    		<h3 className="font-semibold tracking-widest font-aladin text-2xl md:text-4xl -mt-8 md:-mt-10 border-b-2 md:border-b-4 border-gray-800 text-center text-right">
	    			YASH POKAR
	    		</h3>

	    		<div className="flex flex-col text-right mt-3 md:px-5 py-1 md:py-2">
	    			<span className="text-gray-700 tracking-widest uppercase py-1 text-sm md:text-md has-fancy-border-bottom has-fancy-border-bottom--normal font-semibold">Full Stack Developer</span>
	    			<span className="text-gray-700 tracking-widest uppercase py-1 text-sm md:text-md has-fancy-border-bottom has-fancy-border-bottom--larger font-semibold">Bangalore, Karnataka (IN)</span>
	    		</div>

				<div className="flex justify-around mt-3">
					<a href="https://www.linkedin.com/in/yashpokar" target="_blank" className="md:text-xl flex-1 text-center py-2 hover:text-gray-900">
						<FontAwesomeIcon icon={faLinkedin} />
					</a>

					<a href="https://github.com/yashpokar" target="_blank" className="md:text-xl flex-1 text-center py-2 hover:text-black ml-2">
						<FontAwesomeIcon icon={faGithub} />
					</a>

					<a href="http://twitter.com/@yashpokar" target="_blank" className="md:text-xl flex-1 text-center py-2 hover:text-blue-500 ml-2">
						<FontAwesomeIcon icon={faTwitter} />
					</a>

					<a href="https://www.instagram.com/yashpokar/" target="_blank" className="md:text-xl flex-1 text-center py-2 hover:text-pink-600 ml-2">
						<FontAwesomeIcon icon={faInstagram} />
					</a>

					<a href="https://medium.com/@yashpokar" target="_blank" className="md:text-xl flex-1 text-center py-2 hover:text-red-600 ml-2 shadow hover:shadow-lg rounded-sm">
						<FontAwesomeIcon icon={faBlog} />
					</a>
				</div>
	    	</div>
    	</div>
    </div>
  )
}
