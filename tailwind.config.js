module.exports = {
  	purge: ['./components/**/*.js', './pages/**/*.js'],
  	theme: {
    	extend: {
			colors: {
				primary: '#f1c40f'
			},
			fontFamily: {
				roboto: ['Roboto', 'sans-serif'],
				aladin: ['Aladin', 'cursive'],
			}
    	},
	},
	variants: {},
	plugins: [],
}
